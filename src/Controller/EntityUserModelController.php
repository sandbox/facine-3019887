<?php

namespace Drupal\user_models\Controller;

use Drupal\user_models\Entity\UserModelInterface;
use Drupal\Core\Controller\ControllerBase;

/**
 * Controller routines for admin user model entity routes.
 */
class EntityUserModelController extends ControllerBase {

  /**
   * Calls a method on an user model and reloads the listing page.
   *
   * @param \Drupal\user_models\Entity\UserModelInterface $model
   *   The user model being acted upon.
   * @param string $op
   *   The operation to perform, e.g., 'enable' or 'disable'.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect back to the collection page.
   */
  public function performOperation(UserModelInterface $model, $op) {
    $model->$op()->save();
    drupal_set_message($this->t('The user model settings have been updated.'));

    return $this->redirect('entity.user_model.collection');
  }

}
