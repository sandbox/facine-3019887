<?php

namespace Drupal\user_models;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\user\UserInterface;
use Drupal\user_models\Plugin\PermissionTypePluginManagerInterface;
use Ordermind\LogicalPermissions\LogicalPermissions;

/**
 * Provides an access checker service.
 */
class AccessChecker {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Ordermind\LogicalPermissions\LogicalPermissions
   */
  protected $logicalPermissions;

  /**
   * The user model permission type plugin manager.
   *
   * @var \Drupal\user_models\Plugin\PermissionTypePluginManagerInterface
   */
  protected $permissionTypeManager;

  /**
   * Constructs a AccessChecker object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\user_models\Plugin\PermissionTypePluginManagerInterface $permission_type
   *   The user model permission type plugin manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, PermissionTypePluginManagerInterface $permission_type) {
    $this->entityTypeManager = $entity_type_manager;
    $this->permissionTypeManager = $permission_type;

    $this->logicalPermissions = new LogicalPermissions();
    foreach ($this->permissionTypeManager->createInstances() as $instance_id => $instance) {
      $this->logicalPermissions->addType($instance_id, [get_class($instance), 'evaluate']);
    }
  }

  /**
   * Checks access for a permission tree.
   *
   * @param array $permissions
   *   The permission tree to be evaluated.
   * @param \Drupal\user\UserInterface $user
   *   The evaluated user.
   * @return boolean
   *   TRUE if access is granted or FALSE if access is denied.
   */
  public function checkAccess(array $permissions = [], UserInterface $user) {
    return $this->logicalPermissions->checkAccess($permissions, $user);
  }

}
