<?php

namespace Drupal\user_models\Plugin\UserModelPermissionType;

use Drupal\user\Entity\User;
use Drupal\user_models\Plugin\PermissionTypeBase;
use Drupal\user_models\Plugin\PermissionTypeInterface;

/**
 * Provides an user models permission type for user roles.
 *
 * @UserModelPermissionType(
 *   id = "user_role",
 *   label = @Translation("User role"),
 *   description = @Translation("Provides a permission type for user roles."),
 * )
 */
class UserRole extends PermissionTypeBase implements PermissionTypeInterface {

  /**
   * {@inheritdoc}
   */
  public static function evaluate(string $permission, User $user) {
    return $user->hasRole($permission);
  }

  /**
   * {@inheritdoc}
   */
  public static function options() {
    return user_role_names();
  }

}
