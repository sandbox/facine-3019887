<?php

namespace Drupal\user_models\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\user_models\AccessChecker;
use Drupal\user_models\Entity\UserModel as UserModelEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'User model' condition.
 *
 * @Condition(
 *   id = "user_model",
 *   label = @Translation("User model"),
 *   context = {
 *     "user" = @ContextDefinition(
 *       "entity:user",
 *       label = @Translation("User")
 *     )
 *   }
 * )
 */
class UserModel extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The user model access checker service.
   *
   * @var \Drupal\user_models\AccessChecker
   */
  protected $accessChecker;

  /**
   * Constructs a UserModel condition plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\user_models\AccessChecker $access_checker
   *   The user model access checker service.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, AccessChecker $access_checker) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->accessChecker = $access_checker;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('user_models.access_checker')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'user_models' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['user_models'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('When the user has the following user models'),
      '#default_value' => $this->configuration['user_models'],
      '#options' => array_map('\Drupal\Component\Utility\Html::escape', user_models_model_names()),
      '#description' => $this->t('If you select no user models, the condition will evaluate to TRUE for all users.'),
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['user_models'] = $form_state->getValue('user_models');

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    // Use the user model labels. They will be sanitized below.
    $user_models = array_intersect_key(user_models_model_names(), $this->configuration['user_models']);
    if (count($user_models) > 1) {
      $user_models = implode(', ', $user_models);
    }
    else {
      $user_models = reset($user_models);
    }
    if ($this->isNegated()) {
      return $this->t('The user does not meet the conditions of @user_models user models', ['@user_models' => $user_models]);
    }
    else {
      return $this->t('The user meets the conditions of @user_models user models', ['@user_models' => $user_models]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    if (empty($this->configuration['user_models']) && !$this->isNegated()) {
      return TRUE;
    }

    /** @var \Drupal\user\UserInterface $user */
    $user = $this->getContextValue('user');
    /** @var \Drupal\user_models\Entity\UserModelInterface[] $user_models */
    $user_models = UserModelEntity::loadMultiple($this->configuration['user_models']);

    foreach ($user_models as $user_model) {
      if ($this->accessChecker->checkAccess($user_model->getPermissionsTree(), $user)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // Optimize cache context, if a user cache context is provided, only use
    // user.permissions, since that's the only part this condition cares about.
    $contexts = array_merge(['user.permissions'], parent::getCacheContexts());
    array_filter($contexts, function($context) {
      return $context != 'user';
    }, ARRAY_FILTER_USE_KEY);

    return $contexts;
  }

}
