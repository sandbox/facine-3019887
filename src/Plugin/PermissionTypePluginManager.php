<?php

namespace Drupal\user_models\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides a plugin manager for user models permission type annotation plugins.
 */
class PermissionTypePluginManager extends DefaultPluginManager implements PermissionTypePluginManagerInterface {

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/UserModelPermissionType', $namespaces, $module_handler, 'Drupal\user_models\Plugin\PermissionTypeInterface', 'Drupal\user_models\Annotation\UserModelPermissionType');

    $this->alterInfo('user_model_permission_type_info');
    $this->setCacheBackend($cache_backend, 'user_model_permission_type');
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    $instances = $this->createInstances([$plugin_id], $configuration);

    return reset($instances);
  }

  /**
   * {@inheritdoc}
   */
  public function createInstances($plugin_id = [], array $configuration = []) {
    if (empty($plugin_id)) {
      $plugin_id = array_keys($this->getDefinitions());
    }

    $factory = $this->getFactory();
    $plugin_ids = (array) $plugin_id;

    $instances = [];
    foreach ($plugin_ids as $plugin_id) {
      $instances[$plugin_id] = $factory->createInstance($plugin_id, isset($configuration[$plugin_id]) ? $configuration[$plugin_id] : []);
    }

    return $instances;
  }

  /**
   * {@inheritdoc}
   */
  public function getTypesFormOptions() {
    $options = [];
    foreach ($this->getDefinitions() as $permission_id => $permission) {
      $options[$permission_id] = $permission['label']->__toString();
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getTypesValuesFormOptions() {
    $options = [];
    foreach ($this->createInstances() as $instance_id => $instance) {
      /** @var \Drupal\user_models\Plugin\PermissionTypeInterface $instance */
      $label = $instance->getPluginDefinition()['label']->__toString();
      // Reindex the options.
      $values = $instance->options();
      $values = array_combine(
        array_map(
          function($key) use ($label) { return $label . '|' . $key; },
          array_keys($values)
        ),
        $values
      );

      $options[$label] = $values;
    }

    return $options;
  }

}
