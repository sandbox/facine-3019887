<?php

namespace Drupal\user_models\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\user\Entity\User;

/**
 * Defines the interface for user modals permission types.
 *
 * @see \Drupal\user_models\Annotation\PermissionType
 */
interface PermissionTypeInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Evaluates the permission type.
   *
   * @param string $permission
   *   The name of the permission to evaluate.
   * @param \Drupal\user\Entity\User $user
   *   The user to evaluate the permission.
   *
   * @return boolean
   *   The value of the evaluation.
   */
  public static function evaluate(string $permission, User $user);

  /**
   * Returns the possible values.
   *
   * @return array
   *   The options values array.
   */
  public static function options();
}
