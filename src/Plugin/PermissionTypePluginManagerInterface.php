<?php

namespace Drupal\user_models\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Provides an interface defining an user models permission type annotation
 * plugin manager.
 */
interface PermissionTypePluginManagerInterface extends PluginManagerInterface {

  /**
   * Create pre-configured instance of plugins.
   *
   * @param array $id
   *   Either the plugin ID or the base plugin ID of the plugins being
   *   instantiated. Also accepts an array of plugin IDs and an empty array to
   *   load all plugins.
   * @param array $configuration
   *   An array of configuration relevant to the plugin instances. Keyed by the
   *   plugin ID.
   *
   * @return \Drupal\user_models\Plugin\PermissionTypeInterface[]
   *   Fully configured plugin instances.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If an instance cannot be created, such as if the ID is invalid.
   */
  public function createInstances($id = [], array $configuration = []);

  /**
   * Returns an array of key value pairs suitable as '#options' for form elements.
   *
   * @return array
   */
  public function getTypesFormOptions();

  /**
   * Returns an array of key value pairs suitable as '#options' for form elements.
   *
   * @return array
   */
  public function getTypesValuesFormOptions();

}