<?php

namespace Drupal\user_models\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user_models\AccessChecker;
use Drupal\user_models\Plugin\PermissionTypePluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for user model forms.
 */
class UserModelForm extends EntityForm {

  /**
   * The user models access checker service.
   *
   * @var \Drupal\user_models\AccessChecker
   */
  protected $accessChecker;

  /**
   * The current user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $currentUser;

  /**
   * The user model permission type plugin manager.
   *
   * @var \Drupal\user_models\Plugin\PermissionTypePluginManagerInterface
   */
  protected $permissionTypeManager;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a UserModelForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\user_models\Plugin\PermissionTypePluginManagerInterface $permission_type
   *   The user model permission type plugin manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current user.
   * @param \Drupal\user_models\AccessChecker $access_checker
   *   The user models access checker service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RendererInterface $renderer, PermissionTypePluginManagerInterface $permission_type, AccountProxyInterface $account, AccessChecker $access_checker) {
    $this->accessChecker = $access_checker;
    $this->currentUser = $entity_type_manager->getStorage('user')->load($account->id());
    $this->permissionTypeManager = $permission_type;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('renderer'),
      $container->get('plugin.manager.user_models.permission_type'),
      $container->get('current_user'),
      $container->get('user_models.access_checker')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['#attached']['library'][] = 'user_models/user_models.admin';

    /** @var \Drupal\user_models\Entity\UserModelInterface $model */
    $model = $this->entity;
    if ($this->operation == 'add') {
      $form['#title'] = $this->t('Add user model');
    }
    else {
      $form['#title'] = $this->t('Edit %label user model', ['%label' => $model->label()]);
    }

    $form['label'] = [
      '#title' => $this->t('Name'),
      '#type' => 'textfield',
      '#default_value' => $model->label(),
      '#description' => $this->t('The human-readable name of this user model. This name must be unique.'),
      '#required' => TRUE,
      '#size' => 30,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $model->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => ['Drupal\user_models\Entity\UserModel', 'load'],
        'source' => ['label'],
      ],
      '#description' => $this->t('A unique machine-readable name for this user model. It must only contain lowercase letters, numbers, and underscores. This name will be used for constructing the URL of the %user_model_add page, in which underscores will be converted into hyphens.', [
        '%user_model_add' => $this->t('Add user model'),
      ]),
    ];
    $form['description'] = [
      '#title' => $this->t('Description'),
      '#type' => 'textarea',
      '#default_value' => $model->getDescription(),
    ];
    $form['permissions_help'] = [
      '#markup' => $this->t('Learn about operators on <a href="https://github.com/ordermind/logical-permissions-php#logic-gates" target="_blank">logical-permissions library page</a>.'),
    ];
    $form['permissions'] = [
      '#type' => 'table',
      '#prefix' => '<div id="permissions-wrapper">',
      '#suffix' => '</div>',
      '#header' => [
        $this->t('Condition'),
        $this->t('Weight'),
        $this->t('Parent'),
        $this->t('Operations'),
      ],
      '#empty' => $this->t('There are no permissions!'),
      '#tabledrag' => [
        [
          'action' => 'match',
          'relationship' => 'parent',
          'group' => 'row-pid',
          'source' => 'row-id',
          'hidden' => TRUE,
          'limit' => FALSE,
        ],
        [
          'action' => 'depth',
          'relationship' => 'group',
          'group' => 'row-depth',
          'hidden' => FALSE,
        ],
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'row-weight',
        ],
      ],
    ];

    $permissions = $form_state->isRebuilding() ? $form_state->getValue('permissions') : $model->getPermissions();

    foreach ($permissions as $row) {
      $form['permissions'][$row['id']]['#attributes']['class'][] = 'draggable';
      if (isset($row['depth']) && $row['depth'] > 0) {
        $indentation = [
          '#theme' => 'indentation',
          '#size' => $row['depth'],
        ];
      }
      $form['permissions'][$row['id']]['condition'] = [
        '#markup' => $row['name'],
        '#prefix' => !empty($indentation) ? $this->renderer->render($indentation) : '',
      ];
      $form['permissions'][$row['id']]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for ID @id', ['@id' => $row['id']]),
        '#title_display' => 'invisible',
        '#default_value' => $row['weight'],
        '#attributes' => [
          'class' => ['row-weight'],
        ],
      ];
      $form['permissions'][$row['id']]['parent']['pid'] = [
        '#parents' => ['permissions', $row['id'], 'pid'],
        '#type' => 'number',
        '#size' => 3,
        '#min' => 0,
        '#title' => $this->t('Parent ID'),
        '#default_value' => $row['pid'],
        '#attributes' => [
          'class' => ['row-pid'],
        ],
      ];
      $form['permissions'][$row['id']]['operations'] = [
        '#type' => 'submit',
        '#value' => $this->t('Delete'),
        '#name' => $row['id'],
        '#submit' => ['::removeItemAjaxSubmit'],
        '#ajax' => [
          'callback' => '::refreshPermissionsCallback',
          'wrapper' => 'permissions-wrapper',
        ]
      ];
      $form['permissions'][$row['id']]['condition']['id'] = [
        '#parents' => ['permissions', $row['id'], 'id'],
        '#type' => 'hidden',
        '#value' => $row['id'],
        '#attributes' => [
          'class' => ['row-id'],
        ],
      ];
      $form['permissions'][$row['id']]['condition']['type'] = [
        '#parents' => ['permissions', $row['id'], 'type'],
        '#type' => 'hidden',
        '#value' => $row['type'],
        '#attributes' => [
          'class' => ['row-type'],
        ],
      ];
      $form['permissions'][$row['id']]['condition']['name'] = [
        '#parents' => ['permissions', $row['id'], 'name'],
        '#type' => 'hidden',
        '#value' => $row['name'],
        '#attributes' => [
          'class' => ['row-name'],
        ],
      ];
      $form['permissions'][$row['id']]['condition']['value'] = [
        '#parents' => ['permissions', $row['id'], 'value'],
        '#type' => 'hidden',
        '#value' => $row['value'],
        '#attributes' => [
          'class' => ['row-value'],
        ],
      ];
      $form['permissions'][$row['id']]['condition']['depth'] = [
        '#parents' => ['permissions', $row['id'], 'depth'],
        '#type' => 'hidden',
        '#value' => $row['depth'],
        '#attributes' => [
          'class' => ['row-depth'],
        ],
      ];
    }
    $form['table_operations'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['permissions-operations'],
      ],
    ];
    $form['table_operations']['logical_operator'] = [
      '#type' => 'select',
      '#title' => $this->t('Operator'),
      '#title_display' => 'invisible',
      '#options' => [
        'AND' => 'AND',
        'NAND' => 'NAND',
        'OR' => 'OR',
        'NOR' => 'NOR',
        'XOR' => 'XOR',
        'NOT' => 'NOT',
      ],
    ];
    $form['table_operations']['add_logical_operator'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add operator'),
      '#submit' => ['::addItemAjaxSubmit'],
      '#ajax' => [
        'callback' => '::refreshPermissionsCallback',
        'wrapper' => 'permissions-wrapper',
      ]
    ];

    $form['table_operations']['comparison_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Comparison'),
      '#title_display' => 'invisible',
      '#options' => $this->permissionTypeManager->getTypesFormOptions(),
    ];
    $form['table_operations']['add_comparison'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add comparison'),
      '#submit' => ['::addItemAjaxSubmit'],
      '#ajax' => [
        'callback' => '::refreshPermissionsCallback',
        'wrapper' => 'permissions-wrapper',
      ]
    ];
    $form['table_operations']['value'] = [
      '#type' => 'select',
      '#title' => $this->t('Value'),
      '#title_display' => 'invisible',
      '#options' => $this->permissionTypeManager->getTypesValuesFormOptions(),
    ];
    $form['table_operations']['add_value'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add value'),
      '#submit' => ['::addItemAjaxSubmit'],
      '#ajax' => [
        'callback' => '::refreshPermissionsCallback',
        'wrapper' => 'permissions-wrapper',
      ]
    ];

    return $form;
  }

  /**
   * Ajax submit handler for loading more table items.
   */
  public function addItemAjaxSubmit(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    // Convert to tree.
    $permissions = $form_state->getValue('permissions');
    // Calculate the new ID.
    $id = max(array_keys($permissions)) + 1;
    $trigger = $form_state->getTriggeringElement();
    /** @var \Drupal\Core\StringTranslation\TranslatableMarkup $trigger_value */
    $trigger_value = $trigger['#value'];

    switch ($trigger_value->getUntranslatedString()) {
      case 'Add operator':
        $permissions[$id] = [
          'id' => $id,
          'type' => 'operator',
          'name' => $values['logical_operator'],
          'value' => $values['logical_operator'],
        ];
        break;

      case 'Add comparison':
        $permissions[$id] = [
          'id' => $id,
          'type' => 'comparison',
          'name' => $this->t('Comparison type: @type', ['@type' => $this->permissionTypeManager->getTypesFormOptions()[$values['comparison_type']]]),
          'value' => $values['comparison_type'],
        ];
        break;

      case 'Add value':
        $options = $this->permissionTypeManager->getTypesValuesFormOptions();
        list($key, $value) = explode('|', $values['value']);
        $permissions[$id] = [
          'id' => $id,
          'type' => 'value',
          'name' => $this->t('@type: @value', [
            '@type' => $key,
            '@value' => $options[$key][$values['value']]
          ]),
          'value' => $value,
        ];
        break;
    }

    $form_state->setValue('permissions', $permissions);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Ajax submit handler for removing element from table.
   */
  public function removeItemAjaxSubmit(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $permissions = $form_state->getValue('permissions');

    // Update element parent id and depth values.
    $update_depth = function($id, $permissions) use (&$update_depth) {
      foreach ($permissions as $key => $permission) {
        if ($permission['pid'] == $id) {
          $permissions[$key]['depth'] = --$permission['depth'];
          $permissions = $update_depth($permission['id'], $permissions);
        }
      }

      return $permissions;
    };
    $update_parent_and_depth = function($id, $pid) use ($update_depth, &$update_parent_and_depth, $permissions) {
      foreach ($permissions as $key => $permission) {
        if ($permission['pid'] == $id) {
          $permissions[$key]['pid'] = $pid;
          $permissions[$key]['depth'] = --$permission['depth'];
          $permissions = $update_depth($permission['id'], $permissions);
        }
      }

      return $permissions;
    };

    $id = $permissions[$trigger['#name']]['id'];
    $pid = $permissions[$trigger['#name']]['pid'];
    $permissions = $update_parent_and_depth($id, $pid);

    // Delete the permission and save it.
    unset($permissions[$trigger['#name']]);
    $form_state->setValue('permissions', $permissions);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Ajax handler for refresh permissions table.
   */
  public function refreshPermissionsCallback(array &$form, FormStateInterface $form_state) {
    return $form['permissions'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getTriggeringElement()['#value']->getUntranslatedString() != 'Save user model') {
      return;
    }

    parent::validateForm($form, $form_state);

    $id = trim($form_state->getValue('id'));
    // '0' is invalid, since elsewhere we check it using empty().
    if ($id == '0') {
      $form_state->setErrorByName('id', $this->t('Invalid machine-readable name. Enter a name other than %invalid.', ['%invalid' => $id]));
    }

    if (empty($form_state->getValue('permissions'))) {
      $form_state->setErrorByName('permissions', $this->t('Permissions field is required.'));
    }
    else {
      try {
        /** @var \Drupal\user_models\Entity\UserModelInterface $model */
        $model = $this->entity;

        $this->accessChecker->checkAccess($model->getPermissionsTree(), $this->currentUser);
      }
      catch (\Exception $e) {
        $form_state->setErrorByName('permissions', $e->getMessage());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\user_models\Entity\UserModelInterface $model */
    $model = $this->entity;

    $model->set('id', trim($model->id()));
    $model->set('label', trim($model->label()));
    $model->set('description', trim($model->getDescription()));
    $status = $model->save();

    $t_args = ['%name' => $model->label()];
    if ($status == SAVED_UPDATED) {
      drupal_set_message($this->t('The user model %name has been updated.', $t_args));
    }
    elseif ($status == SAVED_NEW) {
      drupal_set_message($this->t('The user model %name has been added.', $t_args));
      $context = array_merge($t_args, [
        'link' => $model->toLink($this->t('View'),
          'collection')->toString()
      ]);
      $this->logger('user_models')->notice('Added user model %name.', $context);
    }
    $form_state->setRedirectUrl($model->toUrl('collection'));
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);

    $actions['submit']['#value'] = $this->t('Save user model');
    $actions['delete']['#value'] = $this->t('Delete user model');

    return $actions;
  }

}
