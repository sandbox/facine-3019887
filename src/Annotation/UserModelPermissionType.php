<?php

namespace Drupal\user_models\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an user models permission type annotation object.
 *
 * @Annotation
 */
class UserModelPermissionType extends Plugin {

  /**
   * The plugin ID of the permission type.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the permission type.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A brief description of the permission type.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description = '';

}
