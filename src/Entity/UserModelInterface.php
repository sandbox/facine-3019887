<?php

namespace Drupal\user_models\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Provides an interface defining an user model type entity.
 */
interface UserModelInterface extends ConfigEntityInterface, EntityDescriptionInterface {

  /**
   * Gets the defined permissions.
   *
   * @return array
   *   The defined permissions.
   */
  public function getPermissions();

  /**
   * Sets the defined permissions.
   *
   * @param array $permissions
   *   The defined permissions.
   *
   * @return $this
   */
  public function setPermissions($permissions);

  /**
   * Gets the prepared permissions tree.
   *
   * E.g.:
   * [
   *   'user_role' => [
   *     'OR' => ['editor', 'sales'],
   *   ],
   * ]
   *
   * @return array
   *   The prepared permissions tree.
   */
  public function getPermissionsTree();

}
