<?php

namespace Drupal\user_models\Entity;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the user model configuration entity.
 *
 * @ConfigEntityType(
 *   id = "user_model",
 *   label = @Translation("User model"),
 *   label_singular = @Translation("User model"),
 *   label_plural = @Translation("User models"),
 *   label_count = @PluralTranslation(
 *     singular = "@count user model",
 *     plural = "@count user models"
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\user_models\Form\UserModelForm",
 *       "edit" = "Drupal\user_models\Form\UserModelForm",
 *       "delete" = "Drupal\user_models\Form\UserModelDeleteForm"
 *     },
 *     "list_builder" = "Drupal\user_models\UserModelListBuilder",
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider"
 *     },
 *   },
 *   admin_permission = "administer user models",
 *   fieldable = FALSE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/people/models/add",
 *     "edit-form" = "/admin/people/models/{user_model}",
 *     "delete-form" = "/admin/people/models/{user_model}/delete",
 *     "enable" = "/admin/people/models/{user_model}/enable",
 *     "disable" = "/admin/people/models/{user_model}/disable",
 *     "collection" = "/admin/people/models"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "permissions"
 *   }
 * )
 */
class UserModel extends ConfigEntityBase implements UserModelInterface {

  /**
   * A brief description of this user model.
   *
   * @var string
   */
  protected $description;

  /**
   * The user model ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The user model label.
   *
   * @var string
   */
  protected $label;

  /**
   * The user model permissions.
   *
   * @var array
   */
  protected $permissions;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPermissions() {
    return $this->permissions;
  }

  /**
   * {@inheritdoc}
   */
  public function setPermissions($permissions) {
    $this->permissions = $permissions;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPermissionsTree() {
    $options_tree = $this->permissionsArrayToOptionsTree($this->getPermissions());

    // Update tree keys and clean values.
    $tree = $this->permissionsTreeUpdateKeys($options_tree);

    return $tree;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // Reset IDS and filter values, first create a tree and next convert again
    // to flat array initializing keys.
    $permissions = $this->getPermissions();
    $permissions = $this->permissionsOptionsTreeToArray($this->permissionsArrayToOptionsTree($permissions));
    $this->setPermissions($permissions);
  }

  /**
   * Convert permissions array to tree.
   *
   * E.g.:
   * [
   *   '0' => [
   *     'type' => 'comparison',
   *     'name' => 'Comparison type: User role',
   *     'value' => 'user_role',
   *     'children' => [
   *       '0' => [
   *         'type' => 'operator',
   *         'name' => 'AND',
   *         'value' => 'AND',
   *         'children' => [
   *           '0' => [
   *             'type' => 'value',
   *             'name' => 'User role: Authenticated user',
   *             'value' => 'authenticated',
   *             'children' => [
   *             ],
   *           ],
   *         ],
   *       ],
   *     ],
   *   ],
   * ]
   *
   * @param array $permissions
   *   The permissions array.
   * @param string $id_key
   *   The ID key name.
   * @param string $pid_key
   *   The parent ID key name.
   * @param string $children_key
   *   The children key name.
   * @param array $fields
   *   The allowed values key names.
   *
   * @return array
   *   The composed permissions tree.
   */
  private function permissionsArrayToOptionsTree(array $permissions, string $id_key = 'id', string $pid_key = 'pid', string $children_key = 'children', array $fields = ['type', 'name', 'value']) {
    $indexed = [];

    // Get the array indexed by the primary id.
    foreach ($permissions as $permission) {
      $indexed[$permission[$id_key]] = $permission;
      $indexed[$permission[$id_key]][$children_key] = [];
    }

    // Build tree.
    $roots = [];
    foreach ($indexed as $id => $permission) {
      $indexed[$permission[$pid_key]][$children_key][$id] =& $indexed[$id];
      if (empty($permission[$pid_key])) {
        $roots[] = $id;
      }
    }

    $tree = [];
    foreach ($roots as $root) {
      $tree[$root] = $indexed[$root];

    }

    // Clean keys.
    $clean_keys = function($element) use ($children_key, $fields, &$clean_keys) {
      $children = [];
      foreach ($element as $item) {
        $child = [$children_key => []];
        foreach ($fields as $field) {
          $child[$field] = empty($item[$field]) ? NULL : $item[$field];
        }

        if ($item[$children_key]) {
          $child[$children_key] = $clean_keys($item[$children_key]);
        }
        $children[] = $child;
      }
      return $children;
    };
    $tree = $clean_keys($tree);

    return $tree;
  }

  /**
   * Convert permissions tree to flat array with parent ID, weight and depth.
   *
   * E.g.:
   * [
   *   '1' => [
   *     'id' => 1,
   *     'pid' => 0,
   *     'weight' => 0,
   *     'depth' => 0,
   *     'type' => 'comparison',
   *     'name' => 'Comparison type: User role',
   *     'value' => 'user_role',
   *   ],
   *   '2' => [
   *     'id' => 2,
   *     'pid' => 1,
   *     'weight' => 0,
   *     'depth' => 1,
   *     'type' => 'operator',
   *     'name' => 'AND',
   *     'value' => 'AND',
   *   ],
   *   '3' => [
   *     'id' => 3,
   *     'pid' => 2,
   *     'weight' => 0,
   *     'depth' => 2,
   *     'type' => 'value',
   *     'name' => 'User role: Authenticated user',
   *     'value' => 'authenticated',
   *   ],
   * ]
   *
   * @param array $permissions
   *   The permissions tree.
   * @param int $pid
   *   The parent ID value.
   * @param string $children_key
   *   The children key name.
   * @param int $depth
   *   The child depth value.
   *
   * @return array
   *   The composed flatted permissions array.
   */
  private function permissionsOptionsTreeToArray(array $permissions, int $pid = 0, int $depth = 0, string $children_key = 'children') {
    static $key = 1;

    $array = [];
    $weight = 0;
    foreach($permissions as $permission) {
      $array[$key] = [
          'id' => $key,
          'pid' => $pid,
          'weight' => $weight,
          'depth' => $depth,
        ] + $permission;
      unset($array[$key++][$children_key]);

      $weight++;
      if (count($permission[$children_key])) {
        $array += $this->permissionsOptionsTreeToArray($permission[$children_key], $array[$key - 1]['id'], $array[$key - 1]['depth'] + 1);
      }
    }

    return $array;
  }

  /**
   * Update permission tree keys and clean values.
   *
   * @param array $permissions_tree
   *   The permissions tree array.
   * @param string $children_key
   *   The children key name.
   *
   * @return array
   *   The composed permissions tree.
   */
  private function permissionsTreeUpdateKeys($permissions_tree, string $children_key = 'children') {
    $tree = [];

    foreach ($permissions_tree as $key => $child) {
      if ($child['type'] != 'value') {
        $tree[$child['value']] = [];
        $key = $child['value'];
      }
      else {
        $tree[$key][] = $child['value'];
      }

      if (count($child[$children_key])) {
        $tree[$key] = $this->permissionsTreeUpdateKeys($child[$children_key], $children_key);
      }
    }

    return $tree;
  }

}
