<?php

namespace Drupal\user_models_entity_access\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;

/**
 * Plugin implementation of the 'user_models_entity_access_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "user_models_entity_access_formatter",
 *   label = @Translation("User models access control"),
 *   field_types = {
 *     "user_models_entity_access"
 *   }
 * )
 */
class UserModelsEntityAccessFormatter extends EntityReferenceLabelFormatter {

}
