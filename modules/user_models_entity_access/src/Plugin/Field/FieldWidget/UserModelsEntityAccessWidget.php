<?php

namespace Drupal\user_models_entity_access\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;

/**
 * Plugin implementation of the 'user_models_entity_access_autocomplete_widget'
 * widget.
 *
 * @FieldWidget(
 *   id = "user_models_entity_access_autocomplete_widget",
 *   label = @Translation("User models access control"),
 *   field_types = {
 *     "user_models_entity_access"
 *   }
 * )
 */
class UserModelsEntityAccessWidget extends EntityReferenceAutocompleteWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element,$form, $form_state);

    $element['operation'] = [
      '#type' => 'radios',
      '#options' => [
        0 => $this->t('View'),
        1 => $this->t('Update'),
        2 => $this->t('Delete'),
      ],
      '#default_value' => isset($items[$delta]->operation) ? $items[$delta]->operation : 0,
      '#required' => TRUE,
    ];

    $element['#attached']['library'][] = 'user_models_entity_access/user_models_entity_access_widget';

    return $element;
  }

}
